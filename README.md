## Cấu trúc project:

###### apps:
- `authentication`: auth with jwf
- `staff`: Test

###### cấu trúc của app:
- các package theo từng chức năng
- `migrations`: thư mục chứa migrations của app
- `utils`: thư mục chứa các function, class phụ trợ
- `admin`: file quản lý admin của app
- `apps`: file đc tạo ra sau câu lệnh tạo app
- `models`: file định nghĩa các model

###### Các file docker đi kèm
- docker-compose-dev.yml: file docker-compose cho môi trường develop (mysql vs phpmyadmin)
- docker-compose-local.yml: file docker-compose cho môi trường local test and build
- docker-compose-prod.yml: file docker-compose cho môi trường production 

## Start project

###### required:
- docker & docker compose
- python >= 3.6

###### cho local debug:
- cài đặt virtualenv
- activate virtualenv
- install tất cả package trong requirements.txt 
- start docker-compose cho môi trường dev: `docker-compose -f docker-compose-dev.yml up`
- cài đặt host file: thêm/sửa host file  

`127.0.0.1 database`

- run server: `./manage.py runserver 127.0.0.1:8000`

###### import data:
- truy cập giao diện quản lý cho mysql () ở đường dẫn: `localhost:8001`
- dùng adminer connect đến database
- import data vào db

## Quy ước response

request thành công: 
+ status code = 2XX (200, 201, 204,...)
+ status = true (ở response)

request thất bại:
+ status code = 4XX (400, 401, 404, 403), 5XX (500, 502)
+ status = false (ở response)

file error_code:



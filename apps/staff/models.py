from django.db import models

# Create your models here.
# Register your models here.
from django.db.models import Model


class Staff(Model):
    staff_code = models.CharField(max_length=5, blank=False)
    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250)
    phone_number = models.CharField(max_length=20)
    address = models.CharField(max_length=250)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

from django.shortcuts import render

# Create your views here.
from apps.staff.models import Staff
from apps.staff.serializers import StaffSerializer
from apps.utils.views_helper import GenericViewSet


class StaffViewSet(GenericViewSet):
    queryset = Staff.objects.all()
    serializer_class = StaffSerializer

from rest_framework import serializers

from apps.authentication.models import User


class EmptySerializer(serializers.Serializer):
    def update(self, instance, validated_data):
        pass
    
    def create(self, validated_data):
        pass


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'password')
    
    def validate(self, attrs):
        instance = User.objects.filter(email=attrs['email'])
        if not instance.exists():
            raise Exception("Not exists email")
        return attrs

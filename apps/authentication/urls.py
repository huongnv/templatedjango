from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^login/$', views.AuthenticationViewSet.as_view(), name='auth-login'),
    url(r'^logout/$', views.LogoutViewSet.as_view(), name='auth-out'),
]

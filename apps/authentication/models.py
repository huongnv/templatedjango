from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.
from django.db.models import Model


class User(AbstractUser):
    email = models.CharField(max_length=500)
    password = models.CharField(max_length=100)
    full_name = models.CharField(max_length=200)
    avatar = models.ImageField(upload_to='avatars')
    is_verified_email = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
